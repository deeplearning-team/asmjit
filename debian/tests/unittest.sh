#!/bin/sh
set -e

mkdir build
cd build
cmake .. -DASMJIT_TEST=ON
make
make test
cd ..
rm -rf build
